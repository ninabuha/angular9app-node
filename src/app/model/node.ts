export interface Node{
    id: number;
    title: string;
    icon?: string;
    children?: Node[];
    about?: any;
    status: boolean;
    isSelected: boolean;
}