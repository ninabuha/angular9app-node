import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Node } from "../model/node";

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit {

  @Input() nodes: Node[];
  @Output() emitter: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteNode(id: number){
    for(let node of this.nodes){
      if(node.id == id){
        this.nodes.pop();
      }
    }
  }

}
