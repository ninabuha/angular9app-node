import { Component } from '@angular/core';
import { Node } from './model/node';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Vezbe2-tree';

  searchField: string = "";

  nodes: Node[] = [{
    id: 1,
    title: "Title1",
    icon: "file.png",
    children: [{id: 2,"title": "ChildTitle1", "icon": "file.png", "children": [{id: 3,"title": "ChildChildTitle1", "children": [{id: 4,"title": "ChildChildChildTitle1", "children": [], "about": "some info1", "status": false, isSelected: false}], "about": "some info1", "status": false, isSelected: false}], "about": "some info1", "status": false, isSelected: false}],
    about: "some info",
    status: false,
    isSelected: false
  },
    {
      id: 5,
      title: "Title2",
      icon: "some_icon2.jpg",
      children: [{id: 6,"title": "ChildTitle2", "icon": "file.png", "children": [], "about": "some info2", "status": false, isSelected: false}],
      about: "some info2",
      status: false,
      isSelected: false
  },
  {
    id: 6,
    title: "Title3",
    icon: "some_icon2.jpg",
    children: [],
    about: "some info2",
    status: false,
    isSelected: false
}]

  search(){
    if(this.searchField === ""){
      for(let node of this.nodes){
        node.isSelected = false;
      }
      return;
    }
    for(let node of this.nodes){
      node.isSelected = false;
    }
    
    let newNodes = [];
    for(let node of this.nodes){
      if(node.title.toLocaleLowerCase().includes(this.searchField.toLocaleLowerCase())){
        newNodes.push(node);
      }
    }
    for(let node of newNodes){
      node.isSelected = true;
    }
  }

  init(){
    this.nodes = [{
      id: 1,
      title: "Title1",
      icon: "file.png",
      children: [{"id": 2, "title": "ChildTitle1", "icon": "file.png", "children": [{"id": 3,"title": "ChildChildTitle1", "children": [{"id": 4,"title": "ChildChildChildTitle1", "children": [], "about": "some info1", "status": false, isSelected: false}], "about": "some info1", "status": false, isSelected: false}], "about": "some info1", "status": false, isSelected: false}],
      about: "some info",
      status: false,
      isSelected: false
    },
      {
        id: 5,
        title: "Title2",
        icon: "some_icon2.jpg",
        children: [{id: 6, "title": "ChildTitle2", "icon": "file.png", "children": [], "about": "some info2", "status": false, isSelected: false}],
        about: "some info2",
        status: false,
        isSelected: false
    },
    {
      id: 6,
      title: "Title3",
      icon: "some_icon2.jpg",
      about: "some info2",
      status: false,
      isSelected: false
  }]
  }
  

}
