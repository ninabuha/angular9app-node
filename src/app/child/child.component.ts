import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Node } from "../model/node";

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() node: Node;
  rightClick: boolean = false;
  titleValue: string;
  isSelected: boolean = false;

  @Output() childEmitter:EventEmitter<Node> = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  nodeClick(){
    this.node.status = !this.node.status;
    if(!this.node.status){
      for(let n of this.node.children){
        n.status = false;
      }
    }
  }

  deleteNode(id){
    this.childEmitter.emit(id);
  }

  onRightClick(){
    this.rightClick = true;
    this.titleValue = this.node.title;
  }

  saveNewTitle(){
    this.node.title = this.titleValue;
    this.rightClick = false;
  }

  selectNode(){
    this.node.isSelected = !this.node.isSelected;
  }

}
